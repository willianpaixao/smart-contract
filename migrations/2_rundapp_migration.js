require('dotenv').config()

const RunDapp = artifacts.require("RunDapp");

module.exports = function (deployer) {
  deployer.deploy(RunDapp, process.env.RECEIVER);
};
