# RunDapp

[![pipeline status](https://gitlab.com/willianpaixao/run-dapp/badges/master/pipeline.svg)](https://gitlab.com/willianpaixao/run-dapp/-/commits/master)

A distributed application used for micro donations and physical activities, using Ethereum ecosystem.

### Getting started
* Requirements
  * [truffle](https://www.trufflesuite.com/)
  * An [Infura](https://infura.io/) API key

### Development
### Testing
``` shell
$ truffle test --network ropsten --verbose-rpc
```
### Deployment
``` shell
$ truffle migrate --network ropsten --verbose-rpc
```
