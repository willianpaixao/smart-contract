// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.0;

contract RunDapp {
    address public owner;
    address payable public receiver;
    
    event Received(address, uint256);
    event Withdrawn(address, uint256);
    
    constructor(address payable _receiver) payable {
        owner = msg.sender;
        receiver = _receiver;
    }
    
    modifier isOwner() {
        require(msg.sender == owner, "Caller is not owner");
        _;
    }
    
    modifier isReceiver() {
        require(msg.sender == receiver, "Caller is not receiver");
        _;
    }
    
    receive() external payable isOwner {
        emit Received(msg.sender, msg.value);
    }
    
    function getBalance() public view returns(uint256) {
        return address(this).balance;
    }
    
    function withdraw(uint256 value) public isReceiver {
        if (getBalance() > value) {
            receiver.transfer(value);
            emit Withdrawn(receiver, value);
        }
    }
}

